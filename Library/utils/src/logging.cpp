/**
 *  @file   logging.cpp
 *  @brief  Different methods used to log data
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <iostream>
#include "logging.hpp"

/**
 *  @name     Logging
 *  @fn       Logging(const std::string file_name)
 *  @brief    Constructor
 *
 *  @param[in] file_name   file_name where the log will be saved,
 *  has to be a .bin file
 */
Logging::Logging(const std::string file_name) :
        file_name_(file_name) {
  std::cout << "New log file: " << file_name_ << std::endl;
}

/**
 *  @name     add
 *  @fn       int add(float& timestamp, ObjectPose& pose)
 *  @brief    adding / pushing a new value into the log stream
 *
 *  @param[in]  timestamp   the time in sec at which the data as been taken
 *  @param[in]  pose        the data to be logged
 * 
 *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
 */
int Logging::add(float& timestamp, ObjectPose& pose) {
  int error = EXIT_FAILURE;
  cv::Mat rotm, t;
  Quaternion quat;

  pose.get_pose_r(&t, &rotm);
  pose.get_quaternion(&quat);

  if (output_stream_.good() && !rotm.empty() && !t.empty()) {
    output_stream_.write(reinterpret_cast<const char *>(&timestamp),sizeof(timestamp));
    output_stream_.write(rotm.ptr<const char>(), rotm.elemSize() * 9);
    output_stream_.write(t.ptr<const char>(), t.elemSize() * 3);
    output_stream_.write(reinterpret_cast<const char*>(&quat.w), sizeof(float));
    output_stream_.write(reinterpret_cast<const char*>(&quat.x), sizeof(float));
    output_stream_.write(reinterpret_cast<const char*>(&quat.y), sizeof(float));
    output_stream_.write(reinterpret_cast<const char*>(&quat.z), sizeof(float));
    error = output_stream_.good() ? EXIT_SUCCESS : EXIT_FAILURE;
  }
  return error;
}

/**
 *  @name     open_stream
 *  @fn       int open_stream()
 *  @brief    starts / opens the stream to log the data
 * 
 *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
 */
int Logging::open_stream() {
  output_stream_.open(file_name_, std::ios::out | std::ios::binary);

  if (!output_stream_.is_open()) {
    std::cerr << "Unable to open file";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/**
 *  @name     close_stream
 *  @fn       int close_stream()
 *  @brief    ends / closes the stream of logged the data
 * 
 *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
 */
int Logging::close_stream() {
  output_stream_.close();
  if (output_stream_.is_open()) {
    std::cerr << "Unable to close file";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}