/**
 *  @file   objectPose.cpp
 *  @brief  Object Pose, used to manage the pose: translation
 *          and rotation according to a frame of an object.
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <iostream>
#include "objectPose.hpp"
#include <opencv2/highgui.hpp> // to print matrices

/**
 *  @name     sng
 *  @fn       sgn(x)
 *  @brief    Macro implementing the signum function
 *
 *  @param[in]  x   input variable to signum function
 *
 *  @return   1 if x > 0, -1 otherwise
 */
#define sgn(x) (x > 0.0f ? 1.0f : -1.0f)

/**
 *  @name     ObjectPose
 *  @fn       ObjectPose()
 *  @brief    Constructor
 *
 */
ObjectPose::ObjectPose() {
  t_ = cv::Mat::zeros(3, 1, cv::DataType<float>::type);
  rotm_ = cv::Mat::eye(3, 3, cv::DataType<float>::type);
}

/**
 *  @name     set_id
 *  @fn       void set_id(const int id)
 *  @brief    set the class member: id_
 *
 *  @param[in]  id  the value to give to id_
 */
void ObjectPose::set_id(const int id) {
  id_ = id;
}

/**
 *  @name     set_translation
 *  @fn       void set_translation(const cv::Mat &t)
 *  @brief    set the class member: t with cv::Mat
 *            object
 *
 *  @param[in]  t   value to give to t
 */
void ObjectPose::set_translation(const cv::Mat &t) {
  t.copyTo(t_);
}

/**
 *  @name     set_translation
 *  @fn       void set_translation(const cv::Mat &t)
 *  @brief    set the class member: t_
 *
 *  @param[in]  tx   value to give to x component of t_
 *  @param[in]  ty   value to give to y component of t_
 *  @param[in]  tz   value to give to z component of t_
 */
void ObjectPose::set_translation(const float tx, const float ty, const float tz) {
  t_ = (cv::Mat_<float>(3,1) << tx, ty, tz);
}

/**
 *  @name     set_rot_matrix
 *  @fn       void set_rot_matrix(const cv::Mat &rotm)
 *  @brief    set the class member: rotm_ with cv::Mat
 *            object, update quat_ member as well
 *
 *  @param[in]  rotm   value to give to rotm_
 */
void ObjectPose::set_rot_matrix(const cv::Mat &rotm) {
  rotm.copyTo(rotm_);

  // Update quaternion
  this->rotm2quat();
}

/**
 *  @name     set_quaternion
 *  @fn       void set_quaternion(const Quaternion &quat)
 *  @brief    set the class member: quat_ with Quaternion
 *            object, update rotm_ member as well
 *
 *  @param[in]  rotm   value to give to rotm_
 */
void ObjectPose::set_quaternion(const Quaternion &quat) {
  quat_.x = quat.x;
  quat_.y = quat.y;
  quat_.z = quat.z;
  quat_.w = quat.w;

  // Update rotation matrix
  this->quat2rotm();
}

/**
 *  @name     set_quaternion
 *  @fn       void set_quaternion(const Quaternion &quat)
 *  @brief    set the class member: quat_ with scalars,
 *            update rotm_ member as well
 *
 *  @param[in]  qw   value to give to w component of quat_
 *  @param[in]  qx   value to give to x component of quat_
 *  @param[in]  qy   value to give to y component of quat_
 *  @param[in]  qz   value to give to z component of quat_
 */
void ObjectPose::set_quaternion(const float qw, const float qx, const float qy, const float qz) {
  quat_.w = qw;
  quat_.x = qx;
  quat_.y = qy;
  quat_.z = qz;

  // Update rotation matrix
  this->quat2rotm();
}

/**
 *  @name     get_id
 *  @fn       const int get_id()
 *  @brief    returns the value of class member: id_
 *
 *  @return   the value of id_
 */
const int ObjectPose::get_id() {
  return id_;
}

/**
 *  @name     get_translation
 *  @fn       void get_translation(cv::Mat *t)
 *  @brief    returns the value of class member: t_
 *
 *  @param[out]   t   the value of t_
 */
void ObjectPose::get_translation(cv::Mat *t) {
  t_.copyTo(*t);
}

/**
 *  @name     get_quaternion
 *  @fn       void get_quaternion(Quaternion *quat)
 *  @brief    returns the value of class member: quat_
 *
 *  @param[out]   quat   the value of quat_
 */
void ObjectPose::get_quaternion(Quaternion *quat) {
  quat->x = quat_.x;
  quat->y = quat_.y;
  quat->z = quat_.z;
  quat->w = quat_.w;
}

/**
 *  @name     get_rot_matrix
 *  @fn       void get_rot_matrix(cv::Mat *rotm)
 *  @brief    returns the value of class member: rotm_
 *
 *  @param[out]   rotm   the value of rotm_
 */
void ObjectPose::get_rot_matrix(cv::Mat *rotm) {
  rotm_.copyTo(*rotm);
}

/**
 *  @name     get_pose_r
 *  @fn       void get_pose_r(cv::Mat *t, cv::Mat *rotm)
 *  @brief    returns the value of class members:
 *            t_ and rotm_
 *
 *  @param[out]   t     the value of t_
 *  @param[out]   rotm  the value of rotm_
 */
void ObjectPose::get_pose_r(cv::Mat *t, cv::Mat *rotm) {
  get_translation(t);
  get_rot_matrix(rotm);
}

/**
 *  @name     get_pose_q
 *  @fn       void get_pose_q(cv::Mat *t, Quaternion *quat)
 *  @brief    returns the value of class members:
 *            t_ and quat_
 *
 *  @param[out]   t     the value of t_
 *  @param[out]   quat  the value of quat_
 */
void ObjectPose::get_pose_q(cv::Mat *t, Quaternion *quat) {
  get_translation(t);
  get_quaternion(quat);
}

/**
 *  @name     quat2rotm
 *  @fn       void quat2rotm()
 *  @brief    update the class member rotm_ with the value
 *            of class member quat_
 */
void ObjectPose::quat2rotm() {
  this->rotm_.at<float>(0,0) = 1.0f - 2.0f * (quat_.y * quat_.y + quat_.z * quat_.z);
  this->rotm_.at<float>(0,1) = 2.0f * (quat_.x * quat_.y - quat_.z * quat_.w);
  this->rotm_.at<float>(0,2) = 2.0f * (quat_.x * quat_.z + quat_.y * quat_.w);

  this->rotm_.at<float>(1,0) = 2.0f * (quat_.x * quat_.y + quat_.z * quat_.w);
  this->rotm_.at<float>(1,1) = 1.0f - 2.0f * (quat_.x * quat_.x + quat_.z * quat_.z);
  this->rotm_.at<float>(1,2) = 2.0f * (quat_.y * quat_.z - quat_.x * quat_.w);

  this->rotm_.at<float>(2,0) = 2.0f * (quat_.x * quat_.z - quat_.y * quat_.w);
  this->rotm_.at<float>(2,1) = 2.0f * (quat_.y * quat_.z + quat_.x * quat_.w);
  this->rotm_.at<float>(2,2) = 1.0f - 2.0f * (quat_.x * quat_.x + quat_.y * quat_.y);
}

/**
 *  @name     rotm2quat
 *  @fn       void rotm2quat()
 *  @brief    update the class member quat_ with the value
 *            of class member rotm_
 */
void ObjectPose::rotm2quat() {

  float eta = 0.0f;

  float   r11 = this->rotm_.at<float>(0,0), r12 = this->rotm_.at<float>(0,1),
          r13 = this->rotm_.at<float>(0,2), r21 = this->rotm_.at<float>(1,0),
          r22 = this->rotm_.at<float>(1,1), r23 = this->rotm_.at<float>(1,2),
          r31 = this->rotm_.at<float>(2,0), r32 = this->rotm_.at<float>(2,1),
          r33 = this->rotm_.at<float>(2,2); 

  if (r11 + r22 + r33 > eta) {
    this->quat_.w = sqrtf( 1.0f + r11 + r22 + r33 ) / 2.0f;
  } else {
    this->quat_.w = sqrtf( (powf(r32-r23,2.0f) + powf(r13-r31,2.0f) + powf(r21-r12,2.0f)) / (3.0f - r11 - r22 - r33) ) / 2.0f;
  }

  if (r11 - r22 - r33 > eta) {
    this->quat_.x = sgn(r32-r23) * sqrtf( 1.0f + r11 - r22 - r33 ) / 2.0f;
  } else {
    this->quat_.x = sgn(r32-r23) * sqrtf( (powf(r32-r23,2.0f) + powf(r12+r21,2.0f) + powf(r31+r13,2.0f)) /
      (3.0f - r11 + r22 + r33) ) / 2.0f;
  }

  if (-r11 + r22 - r33 > eta) {
    this->quat_.y = sgn(r13-r31) * sqrtf( 1.0f - r11 + r22 - r33 ) / 2.0f;
  } else {
    this->quat_.y = sgn(r13-r31) * sqrtf( (powf(r13-r31,2.0f) + powf(r12+r21,2.0f) + powf(r23+r32,2.0f)) /
      (3.0f + r11 - r22 + r33) ) / 2.0f;
  }

  if (-r11 + -r22 + r33 > eta) {
    this->quat_.z = sgn(r21-r12) * sqrtf( 1.0f - r11 - r22 + r33 ) / 2.0f;
  } else {
    this->quat_.z = sgn(r21-r12) * sqrtf( (powf(r21-r12,2.0f) + powf(r31+r13,2.0f) + powf(r32+r23,2.0f)) /
      (3.0f + r11 + r22 - r33) ) / 2.0f;
  }
}

/**
 *  @name     quat2rpy
 *  @fn       void quat2rpy(float *r, float *p, float *y)
 *  @brief    retrieves the euler angle corresponding to the class
 *            member quat_
 *
 *  @param[out] r   euler angle roll
 *  @param[out] p   euler angle pitch
 *  @param[out] y   euler angle yaw
 */
void ObjectPose::quat2rpy(float *r, float *p, float *y) {

  *r = atan2f(2.0f * (quat_.w * quat_.x + quat_.y * quat_.z), 1.0f - 2.0f * (quat_.x * quat_.x + quat_.y * quat_.y));
  *p = asinf(2.0f * (quat_.w * quat_.y - quat_.z * quat_.x));
  *y = atan2f(2.0f * (quat_.w * quat_.z + quat_.x * quat_.y), 1.0f - 2.0f * (quat_.y * quat_.y + quat_.z * quat_.z));
}