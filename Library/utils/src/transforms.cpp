/**
 *  @file   transforms.cpp
 *  @brief  Defines diffents functions to perform on
 *			cv::Mat and Quaternion objects
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include "transforms.hpp"

/**
 *  @name     rotate_vect
 *  @fn       void rotate_vect(const Quaternion& quat, const cv::Mat& src, cv::Mat* dst)
 *  @brief    rotates an input vector using quaternion
 *
 *  @param[in]  quat 	the quaternion defining the rotation
 * 	@param[in] 	src 	the input 3x1 vector
 *  @param[out]	dst 	the rotated vector
 */
void rotate_vect(const Quaternion& quat, const cv::Mat& src, cv::Mat* dst) {
	Quaternion q_src, q_dst;
	dst->create(3,1, cv::DataType<float>::type);

	q_src.x = src.at<float>(0);
	q_src.y = src.at<float>(1);
	q_src.z = src.at<float>(2);
	q_src.w = 0;

	q_dst = q_src * quat.get_conjugate();
	q_dst = quat * q_dst;

	dst->at<float>(0) = q_dst.x;
	dst->at<float>(1) = q_dst.y;
	dst->at<float>(2) = q_dst.z;
}