/**
 *  @file   readFile.cpp
 *  @brief  Defining functions to load files
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <string>
#include <opencv2/core/persistence.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include "readFile.hpp"

/**
 *  @name     loadCalibration
 *  @fn       int loadCalibration(const std::string &calibration_file, cv::Mat *camera_matrix)
 *  @brief    loads the camera calibration information from a YAML file
 * 
 *  param[in]   calibration_file    name of the YAML file with the calibration info
 *  param[out]  camera_matrix       matrix with intrinsic camera parameters
 *
 *  @return     EXIT_SUCCESS if successfully loaded info and EXIT_FAILURE if failed
 */
int loadCalibration(const std::string &calibration_file, cv::Mat *camera_matrix) {

  int ret = EXIT_FAILURE;

  assert(calibration_file.find(".yml") != std::string::npos);

  cv::FileStorage fs(calibration_file, cv::FileStorage::READ);

  fs["camera_matrix"] >> *camera_matrix;

  if (!camera_matrix->empty()) {
    ret = EXIT_SUCCESS;
  }

  return ret;
}