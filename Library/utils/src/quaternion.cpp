/**
 *  @file   quaternion.cpp
 *  @brief  File defining the quaternion class used to
 *          to represent the quaternion along with some
 *          algebra
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <iostream>
#include <math.h>
#include "quaternion.hpp"

/**
 *  @name     Quaternion
 *  @fn       Quaternion()
 *  @brief    Constructor, initializes the quaternion
 *            as null rotation
 */
Quaternion::Quaternion() {
  w = 1;
  x = 0;
  y = 0;
  z = 0;
}

/**
 *  @name     Quaternion
 *  @fn       Quaternion(float w, float x, float y, float z)
 *  @brief    Constructor, initializes the quaternion with given
 *            input values and normalizes it
 *
 *  @param[in]  w   value to give to w part of the quaternion
 *  @param[in]  x   value to give to x part of the quaternion
 *  @param[in]  y   value to give to y part of the quaternion
 *  @param[in]  z   value to give to z part of the quaternion
 *  @warning  actual values may differ if it does not build a unit
 *            quaternion
 */
Quaternion::Quaternion(float w, float x, float y, float z) {
  this->w = w;
  this->x = x;
  this->y = y;
  this->z = z;

  this->normalize();
}

/**
 *  @name     normalize
 *  @fn       void normalize()
 *  @brief    normalizes the quaternion
 */
void Quaternion::normalize() {
  float norm = sqrtf(x * x + y * y + z * z + w * w);
  x /= norm;
  y /= norm;
  z /= norm;
  w /= norm;
}

/**
 *  @name     get_conjugate
 *  @fn       Quaternion get_conjugate() const
 *  @brief    returns the conjugate of the quaternion
 *
 *  @return   quaternion conjugate
 */
Quaternion Quaternion::get_conjugate() const {
  Quaternion q;
  q.x = -this->x;
  q.y = -this->y;
  q.z = -this->z;
  q.w = this->w;

  return q;
}

/**
 *  @name     operator=
 *  @fn       Quaternion& Quaternion::operator=(const Quaternion& quat)
 *  @brief    overload operator = to make quaternion assignment
 *
 *  @usage    quaternion_object_new = quaternion_object_old
 *  @return   a copy of the quaternion object
 */
Quaternion& Quaternion::operator=(const Quaternion& quat) {
  // self-assignment guard
  if (this == &quat)
    return *this;

  this->w = quat.w;
  this->x = quat.x;
  this->y = quat.y;
  this->z = quat.z;

  return *this;
}

/**
 *  @name     operator<<
 *  @fn       std::ostream& operator<<(std::ostream& os, const Quaternion& quat)
 *  @brief    overload operator << to print the value of the quaternion
 *
 *  @usage    std::cout << quaternion_object << std::endl
 *  @results  qw: qw_val, qx: qx_val, qy: qy_val, qz: qz_val
 */
std::ostream& operator<<(std::ostream& os, const Quaternion& quat) {
  return os << "qw: " << quat.w << ", qx: " << quat.x << ", qy: " << quat.y << ", qz: " << quat.z;
}

/**
 *  @name     operator*
 *  @fn       Quaternion operator*(const Quaternion& q_l, const Quaternion& q_r)
 *  @brief    overload operator * to define Hamiltonian quaternion multiplication
 *
 *  @usage    quaternion_object_left * quaternion_object_right
 *
 *  return    result of the multiplication of the two quaternions
 */
Quaternion operator*(const Quaternion& q_l, const Quaternion& q_r) {
  Quaternion q_res;

  q_res.x = q_r.w * q_l.x + q_r.x * q_l.w - q_r.y * q_l.z + q_r.z * q_l.y;
  q_res.y = q_r.w * q_l.y + q_r.x * q_l.z + q_r.y * q_l.w - q_r.z * q_l.x;
  q_res.z = q_r.w * q_l.z - q_r.x * q_l.y + q_r.y * q_l.x + q_r.z * q_l.w;
  q_res.w = q_r.w * q_l.w - q_r.x * q_l.x - q_r.y * q_l.y - q_r.z * q_l.z;

  return q_res;
}