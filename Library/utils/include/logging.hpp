/**
 *  @file   logging.hpp
 *  @brief  Different methods used to log data
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#ifndef TENSEGRITY_POSE_ESTIMATION_LOGGING_HPP
#define TENSEGRITY_POSE_ESTIMATION_LOGGING_HPP

#include <opencv2/core/mat.hpp>
#include <fstream>
#include "objectPose.hpp"

/**
 * @class   Logging
 * @brief   Class used to log data through time in a file
 * @author  Jean-François Burnier
 * @date    03/21/19
 * @ingroup utils
 */
class Logging {
 public:
  /**
   *  @name     Logging
   *  @fn       Logging(const std::string file_name)
   *  @brief    Constructor
   *
   *  @param[in] file_name   file_name where the log will be saved,
   *  has to be a .bin file
   */
  Logging(const std::string file_name);

  /**
   *  @name     add
   *  @fn       int add(float& timestamp, ObjectPose& pose)
   *  @brief    adding / pushing a new value into the log stream
   *
   *  @param[in]  timestamp   the time in sec at which the data as been taken
   *  @param[in]  pose        the data to be logged
   * 
   *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
   */
  int add(float& timestamp, ObjectPose& pose);

  /**
   *  @name     open_stream
   *  @fn       int open_stream()
   *  @brief    starts / opens the stream to log the data
   * 
   *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
   */
  int open_stream();

  /**
   *  @name     close_stream
   *  @fn       int close_stream()
   *  @brief    ends / closes the stream of logged the data
   * 
   *  @return  EXIT_FAILURE if something when wrong, EXIT_SUCCESS if ok
   */
  int close_stream();

 private:

  /** Output file name */
  const std::string file_name_;
  /** Output stream */
  std::ofstream output_stream_;

};

#endif //TENSEGRITY_POSE_ESTIMATION_LOGGING_HPP
