/**
 *  @file   quaternion.hpp
 *  @brief  File defining the quaternion class used to
 *          to represent the quaternion along with some
 *          algebra
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#ifndef TENSEGRITY_POSE_ESTIMATION_QUATERNION_HPP
#define TENSEGRITY_POSE_ESTIMATION_QUATERNION_HPP

#include <iostream>

/**
 * @class   Quaternion
 * @brief   Class used to represent a quaternion and its algebra
 * @author  Jean-François Burnier
 * @date    03/21/19
 * @ingroup utils
 */
class Quaternion {
 public:

  /**
   *  @name     Quaternion
   *  @fn       Quaternion()
   *  @brief    Constructor, initializes the quaternion
   *            as null rotation
   */
  Quaternion();

  /**
   *  @name     Quaternion
   *  @fn       Quaternion(float w, float x, float y, float z)
   *  @brief    Constructor, initializes the quaternion with given
   *            input values and normalizes it
   *
   *  @param[in]  w   value to give to w part of the quaternion
   *  @param[in]  x   value to give to x part of the quaternion
   *  @param[in]  y   value to give to y part of the quaternion
   *  @param[in]  z   value to give to z part of the quaternion
   *  @warning  actual values may differ if it does not build a unit
   *            quaternion
   */
  Quaternion(float w, float x, float y, float z);

  float x;
  float y;
  float z;
  float w;

  /**
   *  @name     normalize
   *  @fn       void normalize()
   *  @brief    normalizes the quaternion
   */
  void normalize();

  /**
   *  @name     get_conjugate
   *  @fn       Quaternion get_conjugate() const
   *  @brief    returns the conjugate of the quaternion
   *
   *  @return   quaternion conjugate
   */
  Quaternion get_conjugate() const;

  /**
   *  @name     operator=
   *  @fn       Quaternion& Quaternion::operator=(const Quaternion& quat)
   *  @brief    overload operator = to make quaternion assignment
   *
   *  @usage    quaternion_object_new = quaternion_object_old
   *  @return   a copy of the quaternion object
   */
  Quaternion& operator=(const Quaternion& quat);

  /**
   *  @name     operator<<
   *  @fn       std::ostream& operator<<(std::ostream& os, const Quaternion& quat)
   *  @brief    overload operator << to print the value of the quaternion
   *
   *  @usage    std::cout << quaternion_object << std::endl
   *  @results  qw: qw_val, qx: qx_val, qy: qy_val, qz: qz_val
   */
  friend std::ostream& operator<<(std::ostream& os, const Quaternion& quat);

  /**
   *  @name     operator*
   *  @fn       Quaternion operator*(const Quaternion& q_l, const Quaternion& q_r)
   *  @brief    overload operator * to define Hamiltonian quaternion multiplication
   *
   *  @usage    quaternion_object_left * quaternion_object_right
   *
   *  return    result of the multiplication of the two quaternions
   */
  friend Quaternion operator*(const Quaternion& q_l, const Quaternion& q_r);
};

#endif //TENSEGRITY_POSE_ESTIMATION_QUATERNION_HPP