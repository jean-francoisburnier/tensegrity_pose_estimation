/**
 *  @file   objectPose.hpp
 *  @brief  Object Pose, used to manage the pose: translation
 *          and rotation according to a frame of an object.
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include "quaternion.hpp"
#include <opencv2/core/core.hpp>

#ifndef TRANSFORMS_HPP
#define TRANSFORMS_HPP

/**
 *  @name     rotate_vect
 *  @fn       void rotate_vect(const Quaternion& quat, const cv::Mat& src, cv::Mat* dst)
 *  @brief    rotates an input vector using quaternion
 *
 *  @param[in]  quat    the quaternion defining the rotation
 *  @param[in]  src     the input 3x1 vector
 *  @param[out] dst     the rotated vector
 */
void rotate_vect(const Quaternion& quat, const cv::Mat& src, cv::Mat* dst);

#endif // TRANSFORMS_HPP