/**
 *  @file   objectPose.hpp
 *  @brief  Object Pose, used to manage the pose: translation
 *          and rotation according to a frame of an object.
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#ifndef TENSEGRITY_POSE_ESTIMATION_OBJECTPOSE_HPP
#define TENSEGRITY_POSE_ESTIMATION_OBJECTPOSE_HPP

#include <opencv2/core/mat.hpp>
#include "quaternion.hpp"

/**
 * @class   ObjectPose
 * @brief   Class used to represent the pose of an object
 *          in terms of position (x,y,z) and orientation
 *          (rotm or quat)
 * @author  Jean-François Burnier
 * @date    03/21/19
 * @ingroup utils
 */
class ObjectPose {
 public:

  /**
   *  @name     ObjectPose
   *  @fn       ObjectPose()
   *  @brief    Constructor
   *
   */
  ObjectPose();

  /**
   *  @name     set_id
   *  @fn       void set_id(const int id)
   *  @brief    set the class member: id_
   *
   *  @param[in]  id  the value to give to id_
   */
  void set_id(const int id);

  /**
   *  @name     set_translation
   *  @fn       void set_translation(const cv::Mat &t)
   *  @brief    set the class member: t with cv::Mat
   *            object
   *
   *  @param[in]  t   value to give to t
   */
  void set_translation(const cv::Mat &t);

  /**
   *  @name     set_translation
   *  @fn       void set_translation(const cv::Mat &t)
   *  @brief    set the class member: t_
   *
   *  @param[in]  tx   value to give to x component of t_
   *  @param[in]  ty   value to give to y component of t_
   *  @param[in]  tz   value to give to z component of t_
   */
  void set_translation(const float tx, const float ty, const float tz);

  /**
   *  @name     set_rot_matrix
   *  @fn       void set_rot_matrix(const cv::Mat &rotm)
   *  @brief    set the class member: rotm_ with cv::Mat
   *            object, update quat_ member as well
   *
   *  @param[in]  rotm   value to give to rotm_
   */
  void set_rot_matrix(const cv::Mat &rotm);

  /**
   *  @name     set_quaternion
   *  @fn       void set_quaternion(const Quaternion &quat)
   *  @brief    set the class member: quat_ with Quaternion
   *            object, update rotm_ member as well
   *
   *  @param[in]  rotm   value to give to rotm_
   */
  void set_quaternion(const Quaternion &quat);

  /**
   *  @name     set_quaternion
   *  @fn       void set_quaternion(const Quaternion &quat)
   *  @brief    set the class member: quat_ with scalars,
   *            update rotm_ member as well
   *
   *  @param[in]  qw   value to give to w component of quat_
   *  @param[in]  qx   value to give to x component of quat_
   *  @param[in]  qy   value to give to y component of quat_
   *  @param[in]  qz   value to give to z component of quat_
   */
  void set_quaternion(const float qw, const float qx, const float qy, const float qz);

  /**
   *  @name     get_id
   *  @fn       const int get_id()
   *  @brief    returns the value of class member: id_
   *
   *  @return   the value of id_
   */
  const int get_id();
  
  /**
   *  @name     get_translation
   *  @fn       void get_translation(cv::Mat *t)
   *  @brief    returns the value of class member: t_
   *
   *  @param[out]   t   the value of t_
   */
  void get_translation(cv::Mat *t);

  /**
   *  @name     get_quaternion
   *  @fn       void get_quaternion(Quaternion *quat)
   *  @brief    returns the value of class member: quat_
   *
   *  @param[out]   quat   the value of quat_
   */
  void get_rot_matrix(cv::Mat *rotm);

  /**
   *  @name     get_rot_matrix
   *  @fn       void get_rot_matrix(cv::Mat *rotm)
   *  @brief    returns the value of class member: rotm_
   *
   *  @param[out]   rotm   the value of rotm_
   */
  void get_quaternion(Quaternion *quat);

  /**
   *  @name     get_pose_r
   *  @fn       void get_pose_r(cv::Mat *t, cv::Mat *rotm)
   *  @brief    returns the value of class members:
   *            t_ and rotm_
   *
   *  @param[out]   t     the value of t_
   *  @param[out]   rotm  the value of rotm_
   */
  void get_pose_r(cv::Mat *t, cv::Mat *rotm);

  /**
   *  @name     get_pose_q
   *  @fn       void get_pose_q(cv::Mat *t, Quaternion *quat)
   *  @brief    returns the value of class members:
   *            t_ and quat_
   *
   *  @param[out]   t     the value of t_
   *  @param[out]   quat  the value of quat_
   */
  void get_pose_q(cv::Mat *t, Quaternion *quat);

  /**
   *  @name     quat2rotm
   *  @fn       void quat2rotm()
   *  @brief    update the class member rotm_ with the value
   *            of class member quat_
   */
  void quat2rpy(float *r, float *p, float *y);

  //private:

  /** An identification number given to the object */
  int id_;
  /** Translation vector representing the position */
  cv::Mat t_;
  /** Rotation matrix representing the orentation */
  cv::Mat rotm_;
  /** Quaternion representing the orentation */
  Quaternion quat_;
 //private:

// TODO: check here who uses it
/**
 *  @name     rotm2quat
 *  @fn       void rotm2quat()
 *  @brief    update the class member quat_ with the value
 *            of class member rotm_
 */
void rotm2quat();

/**
 *  @name     quat2rpy
 *  @fn       void quat2rpy(float *r, float *p, float *y)
 *  @brief    retrieves the euler angle corresponding to the class
 *            member quat_
 *
 *  @param[out] r   euler angle roll
 *  @param[out] p   euler angle pitch
 *  @param[out] y   euler angle yaw
 */
void quat2rotm();

};

#endif //TENSEGRITY_POSE_ESTIMATION_OBJECTPOSE_HPP