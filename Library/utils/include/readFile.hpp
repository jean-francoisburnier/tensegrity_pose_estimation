/**
 *  @file   readFile.cpp
 *  @brief  Defining functions to load files
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#ifndef TENSEGRITY_POSE_ESTIMATION_READFILE_HPP
#define TENSEGRITY_POSE_ESTIMATION_READFILE_HPP

/**
 *  @name     loadCalibration
 *  @fn       int loadCalibration(const std::string &calibration_file, cv::Mat *camera_matrix)
 *  @brief    loads the camera calibration information from a YAML file
 * 
 *  param[in]   calibration_file    name of the YAML file with the calibration info
 *  param[out]  camera_matrix       matrix with intrinsic camera parameters
 *
 *  @return     EXIT_SUCCESS if successfully loaded info and EXIT_FAILURE if failed
 */
int loadCalibration(const std::string& calibration_file, cv::Mat *camera_matrix);

#endif //TENSEGRITY_POSE_ESTIMATION_READFILE_HPP