#include <iostream>

#include "ukf.hpp"
#include "opencv2/highgui/highgui.hpp"

#define STATE_DIM 7
#define MEAS_DIM 7

int main(int argc, char *argv[]) {

	ukf_conf_t config = {
		.state_dim = STATE_DIM,
		.alpha 	= 0.5f,
		.beta 	= 2.0f,
		.kappa 	= 0.0f
	};

	ukf_state_t init_state = {
		.t = (cv::Mat_<float>(3,1) << 0, 0, 0),
		.q = Quaternion(1.0,0.0,0.0,0.0),
		.P = cv::Mat::eye(STATE_DIM-1,STATE_DIM-1, cv::DataType<float>::type)
	};
	
	static ukf_pose_t offset = {
		.t = (cv::Mat_<float>(3,1) << 0, 0, 0),
		.q = Quaternion(1,0,0,0)
	};

	Ukf ukf(config);
	ukf.init(init_state);

	ukf_measurement_t meas1 = {
		.t = (cv::Mat_<float>(3,1) << -10.0, -12.0, -300.0),
		.q = Quaternion(0.5f,0.5f,0.5f,0.5f)
	};

	ukf_measurement_t meas2 = {
		.t = (cv::Mat_<float>(3,1) << -12, -14, -350),
		.q = Quaternion(0.45,0.505,0.505,0.45)
	};

	for (int k = 0; k < 20; k++) {
		std::cout << "MEASURE1" << std::endl;
		ukf.measurement_update(meas1, offset);
		cv::waitKey(200);
	}

	for (int k = 0; k < 100; k++) {
		std::cout << "MEASURE2" << std::endl;
		ukf.measurement_update(meas2, offset);
		cv::waitKey(200);
	}
}
