/**
 *  @file   ukf.cpp
 *  @brief  Unscented Kalman Filter based on the paper:
 * 			A Quaternion-based Unscented Kalman Filter
 *			for Orientation Tracking by Edgar Kraft from
 *			the Physikalisches Institut of the University
 *			of Bonn.
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/core/hal/hal.hpp>
#include "transforms.hpp"

#include "ukf.hpp"

enum State_position {
	TX 	  = 0,
	TY 	  = 1,
	TZ 	  = 2,
	QUAT_W = 3,
	QUAT_X = 4,
	QUAT_Y = 5,
	QUAT_Z = 6
};

/**
 *  @name     Ukf
 *  @fn       Ukf(ukf_conf_t config)
 *  @brief    Constructor
 *
 * 	param[in] config	structure with ukf config
 */
Ukf::Ukf(ukf_conf_t config) :
  L_(config.state_dim),
  alpha_(config.alpha),
  beta_(config.beta),
  kappa_(config.kappa) {

	sig_p.t = cv::Mat::zeros(3,2*(L_-1)+1, cv::DataType<float>::type);
	sig_p.q.resize(2*(L_-1)+1);

	sig_p_meas.t = cv::Mat::zeros(3,2*(L_-1)+1, cv::DataType<float>::type);
	sig_p_meas.q.resize(2*(L_-1)+1);

	O_noise_ = cv::Mat::eye((L_-1),(L_-1),cv::DataType<float>::type) * 0.01f; // remove 1 dim
}

/**
 *  @name     init
 *  @fn       void init(ukf_state_t init_state)
 *  @brief    initializes the Unscented Kalman Filter
 *
 * 	param[in] config	structure with ukf config
 */
void Ukf::init(ukf_state_t init_state){

	lambda_ = powf(alpha_, 2.0f) * (L_-1 + kappa_) - L_+1;
	gamma_ = sqrtf((L_-1) + lambda_);

	wm0_ = lambda_ / ((L_-1) + lambda_);
	wc0_ = lambda_ / ((L_-1) + lambda_) + (1.0f - powf(alpha_,2.0f) + beta_);
	w_ 	 = 1.0f / (2.0f*((L_-1)+lambda_)); // use L_-1?????

	// Set initial state
	this->state = init_state;
}

/**
 *  @name     measurement_update
 *  @fn       void measurement_update(const ukf_measurement_t& meas,
 *									  const ukf_pose_t& offset)
 *  @brief    performs the measurement update step of the UKF
 *
 * 	param[in] meas		the measurement data
 * 	param[in] offset	pose offset between tag and actual frame
 */
void Ukf::measurement_update(const ukf_measurement_t& meas, const ukf_pose_t& offset) {
	cv::Mat Pyy, Pxy, t_meas_est;
	Quaternion q_meas_est;

	cv::Mat rotMat_err;
	cv::Mat t_err;

	cv::Mat tmp1, tmp2, rotVect_err;

	cv::Mat rotVect_state, innovation, update_state;
	Quaternion quat_update;

	std::cout << "Measurement Update with:" << std::endl;
	std::cout << meas.t << std::endl;
	std::cout << meas.q << std::endl;

	this->compute_sigma_points();
	this->measurement_model(offset);

	// Compute quat meas estimate
	compute_quat_mean(state.q, CV_PI / 360.0f, &q_meas_est, &rotMat_err);

	// update mean state
	cv::reduce(sig_p_meas.t, t_meas_est, 1, cv::REDUCE_SUM, cv::DataType<float>::type);
	t_meas_est = w_ * t_meas_est + (wm0_ - w_) * sig_p_meas.t.col(0);

	quat2rotVect(sig_p.q[0] * state.q.get_conjugate(), &rotVect_err);
	cv::vconcat(sig_p_meas.t.col(0) - t_meas_est, rotMat_err.col(0), tmp1);
	cv::vconcat(sig_p.t.col(0) - state.t, rotVect_err, tmp2);

	Pyy = wc0_ * tmp1 * tmp1.t();
	Pxy = wc0_ * tmp2 * tmp1.t();

	for (int k = 1; k < 2*(L_-1)+1; k++) {

		quat2rotVect(sig_p.q[k] * state.q.get_conjugate(), &rotVect_err);
		cv::vconcat(sig_p_meas.t.col(k) - t_meas_est, rotMat_err.col(k), tmp1);
		cv::vconcat(sig_p.t.col(k) - state.t, rotVect_err, tmp2);

		Pyy += w_ * tmp1 * tmp1.t();
		Pxy += w_ * tmp2 * tmp1.t();
	}

	Pyy += O_noise_;

	kalman_gain_ = Pxy * Pyy.inv();

	// Compute innovation
	quat2rotVect(meas.q * q_meas_est.get_conjugate(), &rotVect_state);
	cv::vconcat(meas.t - t_meas_est, rotVect_state, innovation);

	// Update state mean
	update_state = kalman_gain_ * innovation;
	rotVect2quat(update_state.rowRange(3,6), &quat_update);

	state.t = state.t + update_state.rowRange(0,3);
	state.q = quat_update * state.q;

	// Update state covariance
	state.P -= kalman_gain_ * Pyy * kalman_gain_.t();

	std::cout << "State T" << std::endl;
	std::cout << state.t << std::endl;
	std::cout << "State Q" << std::endl;
	std::cout << state.q << std::endl;
	std::cout << "State P" << std::endl;
	std::cout << state.P << std::endl;	
}

/**
 *  @name     compute_sigma_points
 *  @fn       void compute_sigma_points()
 *  @brief    computes the sigma points necessary for the UKF
 */
void Ukf::compute_sigma_points() {
	
	cv::Mat P_srt,t;
	Quaternion quat;
	cv::Mat rotVect;

	// Compute covariance square root
	mat_squareroot(state.P, &P_srt);
	
	state.t.copyTo(sig_p.t.col(0));
	sig_p.q[0] = state.q;

	for (int k = 1; k <= L_-1; k++) {

		// Translation sigma points
		sig_p.t.col(k) 	  	= state.t + gamma_ * P_srt.rowRange(0,3).col(k-1);
		sig_p.t.col(k+L_-1) = state.t - gamma_ * P_srt.rowRange(0,3).col(k-1);

		rotVect = gamma_ * P_srt.rowRange(3,6).col(k-1);
		rotVect2quat(rotVect, &quat);

		// Quaternion sigma points
		sig_p.q[k] 		= state.q * quat;
		sig_p.q[k+L_-1]	= state.q * quat.get_conjugate();

	}
}

/**
 *  @name     measurement_model
 *  @fn       void measurement_model(const ukf_pose_t& offset)
 *  @brief    the measurement model used to transform the current
 *			  state in the measurement space
 *
 *	param[in]	offset	pose offset between tag and actual frame
 */
void Ukf::measurement_model(const ukf_pose_t& offset) {

	cv::Mat tmp;

	for (int k = 0; k < sig_p.t.cols; k++) {

		rotate_vect(sig_p.q[k], offset.t, &tmp);
		sig_p_meas.t.col(k) = tmp + sig_p.t.col(k);

		sig_p_meas.q[k] = sig_p.q[k] * offset.q;
	}
	//std::cout << "Sig P" << sig_p.t << std::endl;
	//std::cout << "Sig P Meas" << sig_p_meas.t << std::endl;
}

/**
 *  @name     mat_squareroot
 *  @fn       void mat_squareroot(const cv::Mat& src, cv::Mat* dst)
 *  @brief    computes the squareroot of a positive definite matrix
 *			  using the Cholesky decomposition
 *
 *	param[in]	src 	the input matrix
 * 	param[out]	dst 	the output matrix
 */
void Ukf::mat_squareroot(const cv::Mat& src, cv::Mat* dst) {

	src.copyTo(*dst);

	cv::hal::Cholesky((float*)dst->ptr(), dst->step, 3, NULL, 0,0);
	dst->at<float>(0,1) = 0.0f;
	dst->at<float>(0,2) = 0.0f;
	dst->at<float>(1,2) = 0.0f;

	/*
	cv::Mat eigVal, eigVect;
	cv::eigen(src, eigVal, eigVect);
	cv::sqrt(eigVal,eigVal);

	*dst = eigVect.t() * cv::Mat::diag(eigVal) * eigVect;
	*/
}

/**
 *  @name     rotVect2quat
 *  @fn       void rotVect2quat(const cv::Mat& rotVect, Quaternion* quat)
 *  @brief    transform a rotation vector into a quaternion
 *
 *	param[in]	rotVect 	the input rotation vector
 * 	param[out]	quat 		the output quaternion
 */
void Ukf::rotVect2quat(const cv::Mat& rotVect, Quaternion* quat) {
	
	float norm;
	cv::Mat rot_axis;

	norm  = (float)cv::norm(rotVect);

	if (norm < 1e-6f) {
		rot_axis = rotVect * 0.0f;
	}
	else {
		rot_axis = sinf(norm / 2) * rotVect / norm;
	}

	quat->w = cosf(norm / 2.0f);
	quat->x = rot_axis.at<float>(0);
	quat->y = rot_axis.at<float>(1);
	quat->z = rot_axis.at<float>(2);
}

/**
 *  @name     quat2rotVect
 *  @fn       void quat2rotVect(const Quaternion& quat, cv::Mat* rotVect)
 *  @brief    transform a quaternion into a rotation vector
 *
 *	param[in]	quat 		the input quaternion
 * 	param[out]	rotVect 	the output rotation vector
 */
void Ukf::quat2rotVect(const Quaternion& quat, cv::Mat* rotVect) {
	cv::Mat rot_axis = (cv::Mat_<float>(3,1) << quat.x,quat.y,quat.z);
	float norm = (float)cv::norm(rot_axis);

	if (norm < 1e-6f) {
		*rotVect = rot_axis * 0.0f;
	}
	else {
		*rotVect = 2.0f * acosf(quat.w) * rot_axis / (float)cv::norm(rot_axis);
	}
}

/**
 *  @name     compute_quat_mean
 *  @fn       void compute_quat_mean(const Quaternion& init_quat, const float& thresh,
									 Quaternion* mean_quat, cv::Mat* rotMat_err)
 *  @brief    computes the mean quaternion from a set of quaternion using a gradient
 *			  descent algorithm
 *
 *	param[in]	init_quat 	the initial quaternion to use in the optimization
 * 	param[in]	thresh 		threshold used as stopping criterion
 */
void Ukf::compute_quat_mean(const Quaternion& init_quat, const float& thresh,
							Quaternion* mean_quat, cv::Mat* rotMat_err) {
	Quaternion quat_err, sig_p_meas_q, adjustment_quat;
	cv::Mat rotVect_err, adjustment_vect;
	float error_norm;
	int i = 0;

	rotMat_err->create(3, 2*(L_-1)+1, cv::DataType<float>::type);

	// Initialize mean quaternion
	*mean_quat = init_quat;
	
	do  { // Maybe also add an exit with number of iteration otherwise too blocking!!!!

		// Process first sigma point
		quat_err = sig_p_meas.q[0] * mean_quat->get_conjugate();
		quat2rotVect(quat_err, &rotVect_err);
		adjustment_vect = wm0_ * rotVect_err;

		rotVect_err.copyTo(rotMat_err->col(0));

		for (int k = 1; k < 2*(L_-1)+1 ; k++) {

			quat_err = sig_p_meas.q[k] * mean_quat->get_conjugate();
			quat2rotVect(quat_err, &rotVect_err);
			adjustment_vect += w_ * rotVect_err;

			rotVect_err.copyTo(rotMat_err->col(k));
		}

		rotVect2quat(adjustment_vect, &adjustment_quat);

		//update quat mean
		*mean_quat = adjustment_quat * *mean_quat; // not really sure here

		//update error norm
		error_norm = (float)cv::norm(adjustment_vect);

		i++;

	} while (error_norm > thresh);

}

/**
 *  @name     ypr2rotm
 *  @fn       void ypr2rotm(const float& yaw, const float& pitch, const float& roll,
 							cv::Mat* rotm)
 *  @brief    returns a rotation matrix from a set of euler angles following the
 *			  YPR order
 *
 *	param[in]	yaw 	euler angle yaw
 *	param[in]	pitch 	euler angle pitch
 *	param[in]	roll 	euler angle roll
 * 	param[out]	rotm 	resulting rotation matrix
 */
void Ukf::ypr2rotm(const float& yaw, const float& pitch, const float& roll,
				   cv::Mat* rotm) {
	
	float cy = cosf(yaw)  , sy = sinf(yaw);
	float cp = cosf(pitch), sp = sinf(pitch);
	float cr = cosf(roll) , sr = sinf(roll);

	rotm->create(3,3, cv::DataType<float>::type);

	rotm->at<float>(0,0) =  cp * cy;
	rotm->at<float>(0,1) = -cp * sy;
	rotm->at<float>(0,2) =  sp;

	rotm->at<float>(1,0) =  cr * sy + sr * sp * cy;
	rotm->at<float>(1,1) =  cr * cy - sr * sp * sy;
	rotm->at<float>(1,2) = -sr * cp;

	rotm->at<float>(2,0) =  sr * sy - cr * sp * cy;
	rotm->at<float>(2,1) =  sr * cy + cr * sp * sy;
	rotm->at<float>(2,2) = 	cr * cp;
}

/**
 *  @name     rotm2ypr
 *  @fn       void Ukf::rotm2ypr(const cv::Mat& rotm,
 								 float* yaw, float* pitch, float* roll)
 *  @brief    returns the euler angles from a rotation matrix following the
 *			  YPR order
 *
 *  param[in]	rotm 	input rotation matrix
 *	param[out]	yaw 	resulting yaw euler angle
 *	param[out]	pitch 	resulting pitch euler angle
 *	param[out]	roll 	resulting roll euler angle
 */
void Ukf::rotm2ypr(const cv::Mat& rotm,
				   float* yaw, float* pitch, float* roll) {

	float cp = sqrtf(powf(rotm.at<float>(0,0),2.0f) + powf(rotm.at<float>(0,1),2.0f));;

	*roll  = -atan2f(rotm.at<float>(1,2), rotm.at<float>(2,2));
	*pitch = -atan2f(-rotm.at<float>(0,2), cp);
	*yaw   = -atan2f(rotm.at<float>(0,1), rotm.at<float>(0,0));
}

/**
 *  @name     wrap_angle
 *  @fn       void wrap_angle(cv::Mat* theta)
 *  @brief    wraps the input angle theta in the
 *			  the correct domain that corresponds
 *			  to the YPR rotation order
 *
 *  param[in/out]	theta 	the angle to wrap
 */
void Ukf::wrap_angle(cv::Mat* theta) {
	if (theta->size() != cv::Size(1,3)) {
		std::cerr << "Not good" << std::endl;
	}

	// Wrapping: yaw, roll are defined between -pi/pi
	// and pitch between -pi/2,pi/2.
	// When yaw or roll goes over the limit, simply wrap it like: pi-theta or -pi + theta
	// When pitch goes over the limit, add pi to yaw and roll and do: pi/2-theta

	float yaw  	= theta->at<float>(0);
	float pitch = theta->at<float>(1);
	float roll 	= theta->at<float>(2);

	if (pitch > CV_PI / 2.0f) {
		pitch = CV_PI - pitch;
		yaw += CV_PI;
		roll+= CV_PI;
	}
	else if (pitch < -CV_PI / 2.0) {
		pitch = -CV_PI - pitch;
		yaw += CV_PI;
		roll+= CV_PI;
	}

	if (yaw > CV_PI) {
		yaw = -2.0f * CV_PI + yaw;
	}
	else if (yaw < -CV_PI) {
		yaw = 2.0f * CV_PI + yaw;
	}

	if (roll > CV_PI) {
		roll = -2.0f * CV_PI + roll;
	}
	else if (roll < -CV_PI) {
		roll = 2.0f * CV_PI + roll;
	}

	theta->at<float>(0) = yaw;
	theta->at<float>(1) = pitch;
	theta->at<float>(2) = roll;
}

/**
 *  @name     quat2ypr
 *  @fn       void quat2ypr(const Quaternion &quat,
 							float *y, float *p, float *r)
 *  @brief    converts a quaternion into the corresponding euler
 *			  angles following the YPR rotation order
 *
 *  param[in]	quat 	input quaternion
 *	param[out]	yaw 	resulting yaw euler angle
 *	param[out]	pitch 	resulting pitch euler angle
 *	param[out]	roll 	resulting roll euler angle
 */
void Ukf::quat2ypr(const Quaternion &quat, float *y, float *p, float *r) {

  *r = atan2f(2.0f * (quat.w * quat.x + quat.y * quat.z), 1.0f - 2.0f * (quat.x * quat.x + quat.y * quat.y));
  *p = asinf(2.0f * (quat.w * quat.y - quat.z * quat.x));
  *y = atan2f(2.0f * (quat.w * quat.z + quat.x * quat.y), 1.0f - 2.0f * (quat.y * quat.y + quat.z * quat.z));
}
