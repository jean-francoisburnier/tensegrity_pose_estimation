/**
 *  @file   ukf.hpp
 *  @brief  Unscented Kalman Filter based on the paper:
 * 			A Quaternion-based Unscented Kalman Filter
 *			for Orientation Tracking by Edgar Kraft from
 *			the Physikalisches Institut of the University
 *			of Bonn.
 *
 *  @author Jean-François Burnier
 *  @date   03/21/19
 *  Copyright (c) 2019 Jean-François Burnier. All rights reserved.
 */

#include <opencv2/core/mat.hpp>
#include "quaternion.hpp"

#ifndef UNSCENTED_KALMAN_FILTER_HPP
#define UNSCENTED_KALMAN_FILTER_HPP

typedef struct {
	int state_dim;
	float alpha;
	float beta;
	float kappa;
} ukf_conf_t;

typedef struct {
	cv::Mat t;
	Quaternion q;
	cv::Mat P;
} ukf_state_t;

typedef struct {
	cv::Mat t;
	std::vector<Quaternion> q;
} sigma_point_t;

typedef struct {
	cv::Mat t;
	Quaternion q;
} ukf_measurement_t;

typedef struct {
	cv::Mat t;
	Quaternion q;
} ukf_pose_t;

/**
 * @class   Ukf
 * @brief   Class implementing a UKF based Edgar Krafts
 *			algorithm
 * @author  Jean-François Burnier
 * @date    03/21/19
 * @ingroup unscentedkalmanfilter
 */
class Ukf {
public:

    /**
     *  @name     Ukf
     *  @fn       Ukf(ukf_conf_t config)
     *  @brief    Constructor
     *
     * 	param[in] config	structure with ukf config
     */
	Ukf(ukf_conf_t config);

    /**
 	 *  @name     init
 	 *  @fn       void init(ukf_state_t init_state)
 	 *  @brief    initializes the Unscented Kalman Filter
 	 *
 	 *	param[in] config	structure with ukf config
 	 */
	void init(ukf_state_t init_state);

	/**
	 *  @name     measurement_update
	 *  @fn       void measurement_update(const ukf_measurement_t& meas,
	 *									  const ukf_pose_t& offset)
	 *  @brief    performs the measurement update step of the UKF
	 *
	 * 	param[in] meas		the measurement data
	 * 	param[in] offset	pose offset between tag and actual frame
	 */
	void measurement_update(const ukf_measurement_t& meas, const ukf_pose_t& offset);

	/**
	 *  @name     compute_sigma_points
	 *  @fn       void compute_sigma_points()
	 *  @brief    computes the sigma points necessary for the UKF
	 */
	void compute_sigma_points();

	/** The state of Kalman Filter */
	ukf_state_t state;
	/** Dimension of state */
	const int L_;

private:
	/**
	 *  @name     measurement_model
	 *  @fn       void measurement_model(const ukf_pose_t& offset)
	 *  @brief    the measurement model used to transform the current
	 *			  state in the measurement space
	 *
	 *	param[in]	offset	pose offset between tag and actual frame
	 */
	void measurement_model(const ukf_pose_t& offset);

	// Moves these to utils?
	/**
	 *  @name     mat_squareroot
	 *  @fn       void mat_squareroot(const cv::Mat& src, cv::Mat* dst)
	 *  @brief    computes the squareroot of a positive definite matrix
	 *			  using the Cholesky decomposition
	 *
	 *	param[in]	src 	the input matrix
	 * 	param[out]	dst 	the output matrix
	 */
	void mat_squareroot(const cv::Mat& src, cv::Mat* dst);

	/**
	 *  @name     rotVect2quat
	 *  @fn       void rotVect2quat(const cv::Mat& rotVect, Quaternion* quat)
	 *  @brief    transform a rotation vector into a quaternion
	 *
	 *	param[in]	rotVect 	the input rotation vector
	 * 	param[out]	quat 		the output quaternion
	 */

	void rotVect2quat(const cv::Mat& rotVect, Quaternion* quat);
	/**
	 *  @name     quat2rotVect
	 *  @fn       void quat2rotVect(const Quaternion& quat, cv::Mat* rotVect)
	 *  @brief    transform a quaternion into a rotation vector
	 *
	 *	param[in]	quat 		the input quaternion
	 * 	param[out]	rotVect 	the output rotation vector
	 */
	void quat2rotVect(const Quaternion& quat, cv::Mat* rotVect);

	/**
	 *  @name     compute_quat_mean
	 *  @fn       void compute_quat_mean(const Quaternion& init_quat, const float& thresh,
										 Quaternion* mean_quat, cv::Mat* rotMat_err)
	 *  @brief    computes the mean quaternion from a set of quaternion using a gradient
	 *			  descent algorithm
	 *
	 *	param[in]	init_quat 	the initial quaternion to use in the optimization
	 * 	param[in]	thresh 		threshold used as stopping criterion
	 */
	void compute_quat_mean(const Quaternion& init_quat, const float& thresh, Quaternion* mean_quat, cv::Mat* rotMat_err);

	/**
	 *  @name     ypr2rotm
	 *  @fn       void ypr2rotm(const float& yaw, const float& pitch, const float& roll,
	 							cv::Mat* rotm)
	 *  @brief    returns a rotation matrix from a set of euler angles following the
	 *			  YPR order
	 *
	 *	param[in]	yaw 	euler angle yaw
	 *	param[in]	pitch 	euler angle pitch
	 *	param[in]	roll 	euler angle roll
	 * 	param[out]	rotm 	resulting rotation matrix
	 */
	void ypr2rotm(const float& roll, const float& pitch, const float& yaw, cv::Mat* rotm);

	/**
	 *  @name     rotm2ypr
	 *  @fn       void Ukf::rotm2ypr(const cv::Mat& rotm,
	 								 float* yaw, float* pitch, float* roll)
	 *  @brief    returns the euler angles from a rotation matrix following the
	 *			  YPR order
	 *
	 *  param[in]	rotm 	input rotation matrix
	 *	param[out]	yaw 	resulting yaw euler angle
	 *	param[out]	pitch 	resulting pitch euler angle
	 *	param[out]	roll 	resulting roll euler angle
	 */
	void rotm2ypr(const cv::Mat& rotm, float* yaw, float* pitch, float* roll);

	/**
	 *  @name     wrap_angle
	 *  @fn       void wrap_angle(cv::Mat* theta)
	 *  @brief    wraps the input angle theta in the
	 *			  the correct domain that corresponds
	 *			  to the YPR rotation order
	 *
	 *  param[in/out]	theta 	the angle to wrap
	 */
	void wrap_angle(cv::Mat* theta);

	/**
	 *  @name     quat2ypr
	 *  @fn       void quat2ypr(const Quaternion &quat,
	 							float *y, float *p, float *r)
	 *  @brief    converts a quaternion into the corresponding euler
	 *			  angles following the YPR rotation order
	 *
	 *  param[in]	quat 	input quaternion
	 *	param[out]	yaw 	resulting yaw euler angle
	 *	param[out]	pitch 	resulting pitch euler angle
	 *	param[out]	roll 	resulting roll euler angle
	 */
	void quat2ypr(const Quaternion &quat, float *y, float *p, float *r);

	/** The Kalman Gain */
	cv::Mat kalman_gain_;
	/** State Sigma Points */
	sigma_point_t sig_p;
	/** Measurement Sigma Points */
	sigma_point_t sig_p_meas;
	/** Measurement Process Noise */
	cv::Mat O_noise_;
	/** alpha parameter of the UKF */
	float alpha_;
	/** beta parameter of the UKF */
	float beta_;
	/** kappa parameter of the UKF */
	float kappa_;
	/** lambda parameter of the UKF */
	float lambda_;
	/** gamma parameter of the UKF */
	float gamma_;
	/** weight for mean estimation */
	float wm0_;
	/** weight for covariance estimation */
	float wc0_;
	/** weight for mean and covariance estimation */
	float w_;
};

#endif //UNSCENTED_KALMAN_FILTER_HPP