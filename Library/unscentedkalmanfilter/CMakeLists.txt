cmake_minimum_required(VERSION 3.7)
project(ukf CXX)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG_MODE")

# Find OpenCV
find_package(OpenCV REQUIRED)

# Add all necessary headers location to the include paths
set(LOCAL_INCLUDE_DIRS "include")
set(Utils_INCLUDE_DIRS "../utils/include")
include_directories(${LOCAL_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS} ${Utils_INCLUDE_DIRS})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)

# Set main file
set(main_src src/test_ukf.cpp)

# List all the source files
set(srcs
        src/ukf.cpp
        src/sigma_point.cpp)

# List all the header files
set(incs
        ${LOCAL_INCLUDE_DIRS}/ukf.hpp
        ${LOCAL_INCLUDE_DIRS}/sigma_point.hpp)

if (NOT TARGET utils)
  add_subdirectory(../utils EXCLUDE_FROM_ALL utils_lib)
endif()

add_executable(test_ukf ${main_src} ${srcs} ${incs})
target_link_libraries(test_ukf ${OpenCV_LIBS} utils)

# Specify files to build
add_library(ukf SHARED ${srcs} ${incs})
# Link application with libraries
target_link_libraries(ukf ${OpenCV_LIBS} utils)