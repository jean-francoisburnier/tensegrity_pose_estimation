//
// Created by Jean-François Burnier on 28.01.19.
//

#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/mat.hpp>
#include <raspicam/raspicam_cv.h>

int main(int argc, char *argv[]) {
	
	raspicam::RaspiCam_Cv cap;
	
	if (!cap.open()) {
		std::cerr << "Failure to Open Camera" << std::endl;
		return EXIT_FAILURE;
	}
	
	cv::Mat frame;
	char c;
	const std::string image_file = "images/im_";
	int k = 0;
	
	while(1) {
		cap.grab();
		cap.retrieve(frame);
		cv::pyrDown(frame,frame);
		cv::imshow("Window",frame);
		
		c = cv::waitKey(33);
		
		if (c == 'g') {
			cv::imwrite(image_file + std::to_string(k) + ".png",frame);
			k++;
		}
		else if(c == 27) {
			break;
		}
	}
	
	cap.release();
	cv::destroyAllWindows();

	return 0;
}
