#include <iostream>
#include <fstream>
#include <chrono>
#include <tagTracker.hpp>
#include <camera/regular_camera.hpp>
#include <objectPose.hpp>
#include <logging.hpp>

using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {
#ifdef DEBUG_MODE
  std::cout << "OpenCV version: " << CV_VERSION << std::endl;
#endif

  RegularCamera cam(macbookpro_camera_config());
  TagTracker tagTracker(cam);
  tagTracker.parse_config(argc, argv);
  tagTracker.init();

  std::vector<Logging> logs;
  logs.reserve(12); // Do this in an other way
  for (int i = 0; i < 12; ++i) {
    logs.push_back(Logging("../../logging/LOG_TAG_" + std::to_string(i) + ".bin"));
    logs[i].open_stream();
  }

  std::vector<tag_pos_t> tag_pos;
  std::vector<ObjectPose> poses;

  auto timestamp_start = std::chrono::high_resolution_clock::now();
  auto timestamp = std::chrono::high_resolution_clock::now();
  std::chrono::duration<float> t_elapsed;
  float timestamp_sec;

  while (true) {

    //tagTracker.task();
    tagTracker.get_tag_pos(&tag_pos);
    tagTracker.get_pose_from_pos(tag_pos, &poses);
    tagTracker.debug_display(tag_pos);

    for (int i = 0; i < tag_pos.size(); i++) {

    std::cout << "Tag with id: " << tag_pos[i].id << std::endl;
    std::cout << "Corners: " << tag_pos[i].corners[0][0] << "\t" << tag_pos[i].corners[0][1] << std::endl;
    std::cout << "Corners: " << tag_pos[i].corners[1][0] << "\t" << tag_pos[i].corners[1][1] << std::endl;
    std::cout << "Corners: " << tag_pos[i].corners[2][0] << "\t" << tag_pos[i].corners[2][1] << std::endl;
    std::cout << "Corners: " << tag_pos[i].corners[3][0] << "\t" << tag_pos[i].corners[3][1] << std::endl;
    std::cout << "\n" << std::endl;
 
    }

    timestamp = std::chrono::high_resolution_clock::now();

    t_elapsed = timestamp - timestamp_start;
    timestamp_sec = t_elapsed.count();

    for (int i = 0; i < poses.size(); i++) {
      cv::Mat t, rotm;
      poses[i].get_pose_r(&t, &rotm);
      std::cout << "ID: " << poses[i].get_id() << std::endl;
      std::cout << "Translation: \n" << t << std::endl;
      std::cout << "Rotation Matrix: \n" << rotm <<std::endl;

      logs[poses[i].get_id()].add(timestamp_sec,poses[i]);
    }

    if (cv::waitKey(30) == 27) {
      break;
    }
  }

#ifdef DEBUG_MODE
  std::cout << "Exiting" << std::endl;
#endif

  return EXIT_SUCCESS;
}
