//
// Created by Jean-François Burnier on 22.01.19.
//

#include <iostream>
#include <sstream>

#include "tagTracker.hpp"
#include <apriltag/tag16h5.h>
#include <apriltag/tag25h7.h>
#include <apriltag/tag25h9.h>
#include <apriltag/tag36artoolkit.h>
#include <apriltag/tag36h10.h>
#include <apriltag/tag36h11.h>
#include <apriltag/tag16h6.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>

TagTracker::TagTracker(Camera& cam, tagTracker_conf_t config) :
        cam_(cam),
        tag_family_(config.tag_family),
        nb_tag_used_(config.nb_tag_used),
        poses_(config.nb_tag_used) {
#ifdef DEBUG_MODE
  std::cout << "New TagTracker" << std::endl;
#endif

  det_ = nullptr;
  detections_ = nullptr;
  tf_ = nullptr;
  td_ = nullptr;
  getopt_ = nullptr;
  cv::Mat(4, 2, cv::DataType<float>::type, config.corners_ref).copyTo(
          corners_ref_);
}

TagTracker::~TagTracker() {
#ifdef DEBUG_MODE
  std::cout << "Destructing TagTracker" << std::endl;
#endif
  
  //cv::imwrite("output_image.png",gray_);

  apriltag_detector_destroy(td_);

  if (!strcmp(tag_family_, "tag36h11"))
    tag36h11_destroy(tf_);
  else if (!strcmp(tag_family_, "tag36h10"))
    tag36h10_destroy(tf_);
  else if (!strcmp(tag_family_, "tag36artoolkit"))
    tag36artoolkit_destroy(tf_);
  else if (!strcmp(tag_family_, "tag25h9"))
    tag25h9_destroy(tf_);
  else if (!strcmp(tag_family_, "tag25h7"))
    tag25h7_destroy(tf_);
  getopt_destroy(getopt_);

}

int TagTracker::init() {
#ifdef DEBUG_MODE
  std::cout << "TagTrack initialized" << std::endl;
#endif

  // famname_ = getopt_get_string(getopt_, "family");
  if (!strcmp(tag_family_, "tag36h11"))
    tf_ = tag36h11_create();
  else if (!strcmp(tag_family_, "tag36h10"))
    tf_ = tag36h10_create();
  else if (!strcmp(tag_family_, "tag36artoolkit"))
    tf_ = tag36artoolkit_create();
  else if (!strcmp(tag_family_, "tag25h9"))
    tf_ = tag25h9_create();
  else if (!strcmp(tag_family_, "tag25h7"))
    tf_ = tag25h7_create();
  else if (!strcmp(tag_family_, "tag16h5"))
    tf_ = tag16h5_create();
  else if (!strcmp(tag_family_, "tag16h6"))
    tf_ = tag16h6_create();
  else {
    std::cerr << "Unrecognized tag family name. Use e.g. \"tag36h11\".\n"
              << std::endl;
    return EXIT_FAILURE;
  }

  td_ = apriltag_detector_create();
  tf_->black_border = (uint32_t) getopt_get_int(getopt_, "border");
  apriltag_detector_add_family(td_, tf_);
  td_->quad_decimate = 2.0; //getopt_get_float(getopt_, "decimate");
  td_->quad_sigma = getopt_get_float(getopt_, "blur");
  td_->nthreads = getopt_get_int(getopt_, "threads");
  td_->debug = getopt_get_bool(getopt_, "debug");
  td_->refine_edges = getopt_get_bool(getopt_, "refine-edges");
  td_->refine_decode = getopt_get_bool(getopt_, "refine-decode");
  td_->refine_pose = getopt_get_bool(getopt_, "refine-pose");

  int output_active = getopt_get_bool(getopt_, "output");

  // Open camera
  if (cam_.open() == EXIT_FAILURE) {
    std::cerr << "Couldn't open video capture device" << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int TagTracker::parse_config(int argc, char *argv[]) {

  getopt_ = getopt_create();

  // Remove unnecessary parsing
  getopt_add_bool(getopt_, 'h', "help", 0, "Show this help");
  getopt_add_bool(getopt_, 'o', "output", 0, "File Output");
  getopt_add_bool(getopt_, 'd', "debug", 0, "Enable debugging output (slow)");
  getopt_add_bool(getopt_, 'q', "quiet", 0, "Reduce output");
  getopt_add_string(getopt_, 'f', "family", "tag36h11", "Tag family to use");
  getopt_add_int(getopt_, '\0', "border", "1", "Set tag family border size");
  getopt_add_int(getopt_, 't', "threads", "1", "Use this many CPU threads");
  getopt_add_float(getopt_, 'x', "decimate", "1.0",
                   "Decimate input image by this factor");
  getopt_add_float(getopt_, 'b', "blur", "0.0",
                   "Apply low-pass blur to input");
  getopt_add_bool(getopt_, '0', "refine-edges", 1,
                  "Spend more time trying to align edges of tags");
  getopt_add_bool(getopt_, '1', "refine-decode", 0,
                  "Spend more time trying to decode tags");
  getopt_add_bool(getopt_, '2', "refine-pose", 0,
                  "Spend more time trying to precisely localize tags");

  if (!getopt_parse(getopt_, argc, argv, 1) ||
      getopt_get_bool(getopt_, "help")) {
    std::cout << "Usage: " << argv[0] << " [options]\n" << std::endl;
    getopt_do_usage(getopt_);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int TagTracker::get_tag_pos(std::vector<tag_pos_t> *tag_pos) {

  cam_ >> frame_;
  cv::cvtColor(frame_, gray_, cv::COLOR_BGR2GRAY);

  image_u8_t im = {.width  = gray_.cols,
          .height = gray_.rows,
          .stride = gray_.cols,
          .buf    = gray_.data
  };

  detections_ = apriltag_detector_detect(td_, &im);

  tag_pos->clear();
  tag_pos->resize(zarray_size(detections_));

  for (int i = 0; i < zarray_size(detections_); i++) {
    zarray_get(detections_, i, &det_);

    float corners[4][2];
    for (int j = 0; j < 4; ++j) {
      tag_pos->at(i).id = det_->id;
      tag_pos->at(i).corners[j][0] = (float) det_->p[j][0];
      tag_pos->at(i).corners[j][1] = (float) det_->p[j][1];
    }
  }

  zarray_destroy(detections_);
  return 1;
}

int TagTracker::get_pose_from_pos(std::vector<tag_pos_t> &tag_pos,
                                  std::vector<ObjectPose> *poses) {
  
  float dd = 100.0f;
  static double nn[] = {0.0, 0.0, 1.0};
  static cv::Mat normal(3, 1, CV_64F, &nn);
  std::vector<cv::Mat> R, Td, N; 
  ObjectPose* pose;
  poses->clear();

  int id_max = this->nb_tag_used_;
  for (int i = 0; i < tag_pos.size(); i++) {
    if (tag_pos[i].id >= id_max) { // do assert ?
      //std::cout << "Bad id" << std::endl;
      continue;
    }

    cv::Mat corners_m = cv::Mat(4, 2, CV_32FC1, tag_pos[i].corners), m, T, rotm;
    cv::Mat h_mat = cv::getPerspectiveTransform(corners_m, corners_ref_);

    //corners_m.convertTo(corners_m, CV_32FC1);

    int n = cv::decomposeHomographyMat(h_mat, cam_.get_camera_matrix(), R, Td, N);
    
    double val[2] = {-1, 100}, res;
    for (int j = 0; j < n; j++) {
      res = cv::norm(R[j] * N[j] - normal);
      if (res < val[1]) {
        val[0] = j;
        val[1] = res;
      }
    }
    if (val[1] > 0.5) {
      //std::cout << "Error here" << std::endl;
    } else {
      Td[val[0]].convertTo(T, CV_32FC1);
      R[val[0]].convertTo(rotm, CV_32FC1);
      float alpha = dd / (1.0f + T.at<float>(2));

      T *= alpha;
      T.at<float>(2) -= dd;
      
      cv::Mat tmp1 = rotm.t() * T;
      tmp1.at<float>(2) = -tmp1.at<float>(2);
      
      cv::Mat tmp2 = rotm.t();
      tmp2.row(0) = -tmp2.row(0);
      tmp2.row(1) = -tmp2.row(1);

      pose = new ObjectPose;
      pose->set_id(tag_pos[i].id);
      pose->set_translation(tmp1);
      pose->set_rot_matrix(tmp2);
      poses->push_back(*pose);

    }
  }
  return 1;
}

void TagTracker::debug_display(std::vector<tag_pos_t> &tag_pos) {
  cv::circle(frame_, cv::Point(313,236), 2, cv::Scalar(0xff,0,0),-1);
  for (int i = 0; i < tag_pos.size(); i++) {
    cv::line(frame_, cv::Point((int) (tag_pos[i].corners[0][0]), (int) (tag_pos[i].corners[0][1])),
             cv::Point((int) tag_pos[i].corners[1][0], (int) tag_pos[i].corners[1][1]),
             cv::Scalar(0xff, 0xff, 0), 2);
    cv::line(frame_, cv::Point((int) (tag_pos[i].corners[0][0]), (int) (tag_pos[i].corners[0][1])),
             cv::Point((int) (tag_pos[i].corners[3][0]), (int) (tag_pos[i].corners[3][1])),
             cv::Scalar(0, 0, 0xff), 2);
    cv::line(frame_, cv::Point((int) (tag_pos[i].corners[1][0]), (int) (tag_pos[i].corners[1][1])),
             cv::Point((int) (tag_pos[i].corners[2][0]), (int) (tag_pos[i].corners[2][1])),
             cv::Scalar(0xff, 0, 0), 2);
    cv::line(frame_, cv::Point((int) (tag_pos[i].corners[2][0]), (int) (tag_pos[i].corners[2][1])),
             cv::Point((int) (tag_pos[i].corners[3][0]), (int) (tag_pos[i].corners[3][1])),
             cv::Scalar(0, 0xff, 0), 2);

    std::stringstream ss;
    ss << tag_pos[i].id;
    std::string text = ss.str();
    int fontface = cv::FONT_HERSHEY_SCRIPT_SIMPLEX;
    double fontscale = 1.0;
    int baseline;
    cv::Size textsize = cv::getTextSize(text, fontface, fontscale, 2,
                                        &baseline);

    float c_x = ( tag_pos[i].corners[0][0] + tag_pos[i].corners[1][0] +
                tag_pos[i].corners[2][0] + tag_pos[i].corners[3][0] ) / 4.0f;

    float c_y = ( tag_pos[i].corners[0][1] + tag_pos[i].corners[1][1] +
                tag_pos[i].corners[2][1] + tag_pos[i].corners[3][1] ) / 4.0f;

    //putText(frame_, text, cv::Point((int) (c_x - textsize.width  / 2),
    //                               (int) (c_y + textsize.height / 2)),
    //        fontface, fontscale, cv::Scalar(0xff, 0x99, 0), 2);
  }
  cv::imshow("Window", frame_);
}
