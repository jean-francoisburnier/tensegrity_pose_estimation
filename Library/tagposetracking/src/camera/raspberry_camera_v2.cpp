//
// Created by Jean-François Burnier on 29.01.19.
//

#include <camera/raspberry_camera_v2.hpp>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"

int RaspberryCameraV2::open() {
#ifdef DEBUG_MODE
  std::cout << "Opening Raspberry Pi Camera V2" << std::endl;
#endif
  int ret = EXIT_FAILURE;
  
  this->cap_.set(CV_CAP_PROP_FORMAT, CV_8UC3);
  
  if (this->cap_.open()) {
    ret = EXIT_SUCCESS;
  }

  return ret;
}

int RaspberryCameraV2::close() {
#ifdef DEBUG_MODE
  std::cout << "Closing Raspberry Pi Camera V2" << std::endl;
#endif

  this->cap_.release();

  return EXIT_SUCCESS;
}

void RaspberryCameraV2::operator>>(CV_OUT cv::Mat& dst) {
  this->cap_.grab();
  this->cap_.retrieve(dst);
  cv::pyrDown(dst,dst);
}
