//
// Created by Jean-François Burnier on 28.01.19.
//

#include <cstdlib>
#include <camera/camera.hpp>
#include <opencv2/opencv.hpp>
#include <readFile.hpp>

Camera::Camera(camera_config_t config):
  type_(config.type) {
#ifdef DEBUG_MODE
  std::cout << "New Camera Object" << std::endl;
#endif
    if(loadCalibration(config.calibration_file, &camera_matrix_) == -1) {
      std::cerr << "Error with camera configuration loading" << std::endl;
    }
}

cv::Mat& Camera::get_camera_matrix() {
  return this->camera_matrix_;
}