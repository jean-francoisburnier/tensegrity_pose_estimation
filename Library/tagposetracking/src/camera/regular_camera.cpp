//
// Created by Jean-François Burnier on 29.01.19.
//

#include "camera/regular_camera.hpp"

RegularCamera::~RegularCamera() {
  std::cout << "Destructing Regular Camera Object" << std::endl;
  this->close();
}

int RegularCamera::open() {
#ifdef DEBUG_MODE
  std::cout << "Opening Regular Camera" << std::endl;
#endif
  int res = EXIT_FAILURE;

  cap_.open(CV_CAP_ANY);
  if (cap_.isOpened()) {
    res = EXIT_SUCCESS;
  }
  return res;
}

int RegularCamera::close() {
#ifdef DEBUG_MODE
  std::cout << "Closing Regular Camera" << std::endl;
#endif
  int res = EXIT_FAILURE;

  cap_.release();
  if (!cap_.isOpened()) {
    res = EXIT_SUCCESS;
  }
  return res;
}

void RegularCamera::operator>>(CV_OUT cv::Mat& dst) {
  this->cap_.grab();
  this->cap_.retrieve(dst);
  //this->cap_ >> dst;
}