//
// Created by Jean-François Burnier on 22.01.19.
//

#ifndef TENSEGRITY_POSE_ESTIMATION_TAGTRACKER_HPP
#define TENSEGRITY_POSE_ESTIMATION_TAGTRACKER_HPP

#include <apriltag/apriltag.h>
#include <apriltag/getopt.h>
#include <opencv2/opencv.hpp>
#include <camera/camera.hpp>
#include "objectPose.hpp"
#include "logging.hpp"

typedef struct
{
  const char *tag_family;
  const uint8_t nb_tag_used;
  float corners_ref[4][2];
} tagTracker_conf_t;

typedef struct
{
  int id;
  float corners[4][2];
} tag_pos_t;

static inline tagTracker_conf_t tagTracker_default_config();
static inline tagTracker_conf_t tagTracker_25h9_config();

class TagTracker {
 public:
  TagTracker(Camera& cam, tagTracker_conf_t config = tagTracker_default_config());
  ~TagTracker();
  int init();
  int parse_config(int argc, char *argv[]);
  int get_tag_pos(std::vector<tag_pos_t> *tag_pos);
  int get_pose_from_pos(std::vector<tag_pos_t> &tag_pos, std::vector<ObjectPose> *poses);
  void debug_display(std::vector<tag_pos_t> &tag_pos);
 private:
  apriltag_family_t    *tf_;
  apriltag_detector_t  *td_;
  apriltag_detection_t *det_;
  getopt_t             *getopt_;
  zarray_t             *detections_;
  const char           *tag_family_;
  const uint8_t        nb_tag_used_;

  cv::Mat           frame_;
  cv::Mat           gray_;

  cv::Mat corners_ref_;

  std::vector<ObjectPose> poses_;
  ObjectPose pose_;
  Camera&    cam_;
  std::vector<Logging>    log_;

};

static inline tagTracker_conf_t tagTracker_default_config() {
  tagTracker_conf_t conf = {
          .tag_family  = "tag16h6",
          .nb_tag_used = 12,
          .corners_ref = {  544.5944f, 18.7356f,
                            123.8480f, 18.7356f,
                            123.8480f, 439.4820f,
                            544.5944f, 439.4820f
                          }
  };

  return conf;
}

static inline tagTracker_conf_t tagTracker_25h9_config() {
  tagTracker_conf_t conf = {
          .tag_family  = "tag25h9",
          .nb_tag_used = 12,
          .corners_ref = {  172.6004f, 385.6012f,
                            485.1437f, 385.6012f,
                            485.1437f, 73.0578f,
                            172.6004f, 73.0578f
                          }
  };

  return conf;
}

#endif //TENSEGRITY_POSE_ESTIMATION_TAGTRACKER_HPP
