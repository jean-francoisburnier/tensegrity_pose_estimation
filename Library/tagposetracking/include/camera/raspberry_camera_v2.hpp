//
// Created by Jean-François Burnier on 29.01.19.
//

#ifndef TENSEGRITY_POSE_ESTIMATION_RASPBERRY_CAMERA_V2_HPP
#define TENSEGRITY_POSE_ESTIMATION_RASPBERRY_CAMERA_V2_HPP

#include "camera.hpp"
#include <raspicam/raspicam_cv.h>

class RaspberryCameraV2: public Camera {
 public:
  using Camera::Camera;
  int open();
  int close();
  void operator>>(CV_OUT cv::Mat& dst);
  
 private:
 raspicam::RaspiCam_Cv cap_;
};

#endif //TENSEGRITY_POSE_ESTIMATION_RASPBERRY_CAMERA_V2_HPP
