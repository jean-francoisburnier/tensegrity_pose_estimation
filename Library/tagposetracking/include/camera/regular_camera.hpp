//
// Created by Jean-François Burnier on 29.01.19.
//

#ifndef TENSEGRITY_POSE_ESTIMATION_REGULAR_CAMERA_HPP
#define TENSEGRITY_POSE_ESTIMATION_REGULAR_CAMERA_HPP

#include <opencv2/opencv.hpp>
#include "camera.hpp"

class RegularCamera: public Camera {
 public:
  using Camera::Camera;
  ~RegularCamera();

  int open();
  int close();
  void operator>>(CV_OUT cv::Mat& dst);
 private:

  cv::VideoCapture cap_;

};

#endif //TENSEGRITY_POSE_ESTIMATION_REGULAR_CAMERA_HPP
