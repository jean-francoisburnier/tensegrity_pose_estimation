//
// Created by Jean-François Burnier on 28.01.19.
//

#ifndef TENSEGRITY_POSE_ESTIMATION_CAMERA_HPP
#define TENSEGRITY_POSE_ESTIMATION_CAMERA_HPP

#include <string>
#include <opencv2/core/mat.hpp>

typedef enum {
  RASPICAM_V2,
  REGULAR_CAM
} camera_type_t;

typedef struct {
  camera_type_t type;
  const std::string calibration_file;
} camera_config_t;

static inline camera_config_t macbookpro_camera_config();
static inline camera_config_t logitech_camera_config();
static inline camera_config_t raspberry_camera_config();

class Camera {
 public:
  Camera(camera_config_t config);

  cv::Mat& get_camera_matrix();

  virtual int open() = 0;
  virtual int close() = 0;
  virtual void operator>>(CV_OUT cv::Mat& dst) = 0;

 private:
  camera_type_t type_;
  cv::Mat camera_matrix_;
};

static inline camera_config_t macbookpro_camera_config() {
  camera_config_t conf = {
          .type = REGULAR_CAM,
          // .calibration_file = "../../../src/tensegrity_state_estimation/Library/tagposetracking/config/calibration_mac.yml"
          .calibration_file = "../../../config/calibration_mac.yml"
  };

  return conf;
}

static inline camera_config_t logitech_camera_config() {
  camera_config_t conf = {
          .type = REGULAR_CAM,
          .calibration_file = "../../../config/calibration_logitech.yml"
  };

  return conf;
}

static inline camera_config_t raspberry_camera_config() {
  camera_config_t conf = {
          .type = RASPICAM_V2,
          .calibration_file = "../../../config/calibration_raspicam.yml"
  };

  return conf;
}

#endif //TENSEGRITY_POSE_ESTIMATION_CAMERA_HPP
