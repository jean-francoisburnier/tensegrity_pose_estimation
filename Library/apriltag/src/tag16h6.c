/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

An unlimited license is granted to use, adapt, modify, or embed the 2D
barcodes into any medium.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include "apriltag/apriltag.h"

apriltag_family_t *tag16h6_create()
{
   apriltag_family_t *tf = calloc(1, sizeof(apriltag_family_t));
   tf->name = strdup("tag16h6");
   tf->black_border = 1;
   tf->d = 4;
   tf->h = 6;
   tf->ncodes = 16;
   tf->codes = calloc(30, sizeof(uint64_t));
   tf->codes[0] = 0x000000000000192bUL;
   tf->codes[1] = 0x00000000000024b5UL;
   tf->codes[2] = 0x0000000000003bc9UL;
   tf->codes[3] = 0x0000000000004753UL;
   tf->codes[4] = 0x000000000000f80aUL;
   tf->codes[5] = 0x0000000000002bf7UL;
   tf->codes[6] = 0x0000000000007875UL;
   tf->codes[7] = 0x00000000000086f9UL;
   tf->codes[8] = 0x000000000000d93cUL;
   tf->codes[9] = 0x000000000000aa6dUL;
   tf->codes[10] = 0x000000000000efa9UL;
   tf->codes[11] = 0x00000000000059d6UL;
   tf->codes[12] = 0x0000000000004c28UL;
   tf->codes[13] = 0x000000000000811aUL;
   tf->codes[14] = 0x000000000000e54eUL;
   tf->codes[15] = 0x0000000000008237UL;
   return tf;
}

void tag16h6_destroy(apriltag_family_t *tf)
{
   free(tf->name);
   free(tf->codes);
   free(tf);
}
