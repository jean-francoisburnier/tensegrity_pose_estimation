#include "ros/ros.h"
#include "std_msgs/String.h"
#include "tensegrity_state_estimation/Pose.h"
// #include "camera/regular_camera.hpp"
#include "camera/raspberry_camera_v2.hpp"
#include "tagTracker.hpp"
#include "logging.hpp"
#include "transforms.hpp"

#include <XmlRpc.h>
#include <XmlRpcValue.h>

#include <sstream>

static tensegrity_state_estimation::Pose node_pose;
static ObjectPose module_pose;

static int module_id;
static bool flag = false;

void chatterCallback(tensegrity_state_estimation::Pose msg)
{
  ROS_INFO("Hello");
  module_pose.set_id(msg.id);
  module_pose.set_translation(msg.x, msg.y, msg.z);
  module_pose.set_quaternion(msg.qw, msg.qx, msg.qy, msg.qz);
  flag = true;
}

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "tag_pose_estimation");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;
  
  const std::string node_name = ros::this_node::getName();
  if (!n.getParam(node_name + "/module_id", module_id)) {
    std::cerr << "Could not read parameter" << std::endl;
    return EXIT_FAILURE;
  }
  
  ros::Subscriber sub  = n.subscribe<tensegrity_state_estimation::Pose>("module_pose_" + std::to_string(module_id), 1000, chatterCallback);

  std::vector<ros::Publisher> publishers(6);
  const std::string pub_name[6] = {"visual_pose_0" ,"visual_pose_1","visual_pose_2","visual_pose_3","visual_pose_4",
                                   "visual_pose_5"};

  for (int i = 0; i < 6; i++) {
    publishers[i] = n.advertise<tensegrity_state_estimation::Pose>(pub_name[i], 1000);
  }

  int convert_id[12] = {0,0,1,1,2,2,3,3,4,4,5,5};

  ros::Rate loop_rate(30);
  
  camera_config_t cam_conf = {
    .type = RASPICAM_V2,
    .calibration_file = "../../../src/tensegrity_pose_estimation/Library/tagposetracking/config/calibration_raspicam.yml"
  };

  // RegularCamera cam(macbookpro_camera_config());
  RaspberryCameraV2 cam(cam_conf);
  TagTracker tagTracker(cam, tagTracker_25h9_config());
  tagTracker.parse_config(argc, argv);
  tagTracker.init();

  std::vector<tag_pos_t> tag_pos;
  std::vector<ObjectPose> poses;

  std::vector<Logging> logs;
  logs.reserve(12); // Do this in an other way
  for (int i = 0; i < 12; ++i) {
    logs.push_back(Logging("../../../src/tensegrity_pose_estimation/logging/LOG_TAG_" + std::to_string(i) + ".bin"));
    logs[i].open_stream();
  }

  std::vector<int> t_init;
  std::vector<float> q_init;
  
  if (!n.getParam(node_name + "/t_init", t_init) ||
      !n.getParam(node_name + "/q_init", q_init)) {
    std::cerr << "Could not read parameter" << std::endl;
    return EXIT_FAILURE;
  }

  int tx, ty, tz;
  module_pose.set_id(module_id);
  module_pose.set_translation(t_init[0], t_init[1], t_init[2]);
  module_pose.set_quaternion(q_init[0], q_init[1], q_init[2], q_init[3]);

  float timestamp = 0.0f;

  cv::waitKey(100);
  while (ros::ok())
  {
    /**
     * This is a message object. You stuff it with data, and then publish it.
     */
    tensegrity_state_estimation::Pose pose_msg;

    tagTracker.get_tag_pos(&tag_pos);
    tagTracker.get_pose_from_pos(tag_pos, &poses);
    //tagTracker.debug_display(tag_pos);

    /*    
    for (int i = 0; i < tag_pos.size(); i++) {
      std::cout << "Tag with id: " << tag_pos[i].id << std::endl;
      std::cout << "Corners: " << tag_pos[i].corners[0][0] << "\t" << tag_pos[i].corners[0][1] << std::endl;
      std::cout << "Corners: " << tag_pos[i].corners[1][0] << "\t" << tag_pos[i].corners[1][1] << std::endl;
      std::cout << "Corners: " << tag_pos[i].corners[2][0] << "\t" << tag_pos[i].corners[2][1] << std::endl;
      std::cout << "Corners: " << tag_pos[i].corners[3][0] << "\t" << tag_pos[i].corners[3][1] << std::endl;
      std::cout << "\n" << std::endl;
    }
    */
    

    for (int i = 0; i < poses.size(); i++) {
      cv::Mat t, rotm, tmp;
      Quaternion quat;
      float r,p,y;
      poses[i].get_rot_matrix(&rotm);
      poses[i].get_pose_q(&t, &quat);
      poses[i].quat2rpy(&r,&p,&y);

      std::cout << "ID: " << poses[i].get_id() << std::endl;
      std::cout << "Translation: \n" << t << std::endl;
      std::cout << "Rotation Matrix: \n" << rotm << std::endl;
      std::cout << "Quaternion: \n" << quat << std::endl;
      std::cout << "r:" << r * 180.0f / CV_PI << ", p:" << p * 180.0f / CV_PI<< ", y:" << y * 180.0f / CV_PI << std::endl;
      std::cout << "\n\n***********************************************\n" <<std::endl;

      // transform pose using frame of module!!!!

      cv::Mat t_module;
      Quaternion q_module;
      module_pose.get_pose_q(&t_module, &q_module);
      rotate_vect(q_module, t, &tmp);
      tmp += t_module;
      quat = q_module * quat;

      pose_msg.id = poses[i].get_id();
      pose_msg.x = tmp.at<float>(0);
      pose_msg.y = tmp.at<float>(1);
      pose_msg.z = tmp.at<float>(2);
      pose_msg.qx = quat.x;
      pose_msg.qy = quat.y;
      pose_msg.qz = quat.z;
      pose_msg.qw = quat.w;

      logs[poses[i].get_id()].add(timestamp,poses[i]);
      publishers[convert_id[poses[i].get_id()]].publish(pose_msg);
    }
    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    //new_pose_pub.publish(pose_msg);

    //ROS_INFO("OOh x: %f, y: %f, z: %f, qx: %f, qy: %f, qz: %f, qw: %f", node_pose.x, node_pose.y, node_pose.z, node_pose.qx, node_pose.qy, node_pose.qz, node_pose.qw);

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}
