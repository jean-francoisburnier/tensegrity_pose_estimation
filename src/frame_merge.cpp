#include "ros/ros.h"
#include "std_msgs/String.h"
#include "tensegrity_state_estimation/Pose.h"
#include "ukf.hpp"
#include "quaternion.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "transforms.hpp"

#include <sstream>

#define STATE_DIM 7

static bool g_semaphore = false;

static std::vector<Quaternion> quat = {
    Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0),
    Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0),
    Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0), Quaternion(1,0,0,0)
};
static std::vector<cv::Mat> t = {
    (cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),
    (cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),
    (cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),
    (cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0),(cv::Mat_<float>(3,1) << 0, 0, 0)
};

static ukf_pose_t module_offset = {
  .t = (cv::Mat_<float>(3,1) << 102, -27, 0),
  .q = Quaternion(0,0,1,0)
};

static ukf_conf_t config = {
  .state_dim = STATE_DIM,
  .alpha  = 0.5f,
  .beta   = 2.0f,
  .kappa  = 0.0f
};

static Ukf ukf(config);

static tensegrity_state_estimation::Pose module_pose;
static int module_id, bar_id;

void chatterCallback(tensegrity_state_estimation::Pose msg)
{

  ukf_measurement_t meas = {
    .t = (cv::Mat_<float>(3,1) << msg.x, msg.y, msg.z),
    .q = Quaternion(msg.qw, msg.qx, msg.qy, msg.qz)
  };

  ukf_pose_t offset = {
    .t = t[msg.id],
    .q = quat[msg.id]
  };

  # if 1
  ukf.measurement_update(meas, offset); // send here the right pose to get the bar pose and not the tag pose
  #endif
  std::cout << "\n************************************************\n" << std::endl;


  cv::Mat tmp_t;
  Quaternion tmp_q;

  rotate_vect(ukf.state.q, module_offset.t, &tmp_t);
  tmp_t += ukf.state.t;
  module_pose.x = tmp_t.at<float>(0);
  module_pose.y = tmp_t.at<float>(1);
  module_pose.z = tmp_t.at<float>(2);

  tmp_q = ukf.state.q * module_offset.q;
  module_pose.qw = tmp_q.w;
  module_pose.qx = tmp_q.x;
  module_pose.qy = tmp_q.y;
  module_pose.qz = tmp_q.z;

  module_pose.id = module_id;

  g_semaphore = true;
}

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "frame_merge");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  //ros::Publisher new_pose_pub = n.advertise<tensegrity_state_estimation::Pose>("new_pose", 1000);
  //ros::Subscriber other_frame_sub  = n.subscribe<tensegrity_state_estimation::Pose>("own_frame", 1000, chatterCallback);

  const std::string node_name = ros::this_node::getName();
  if(!n.getParam(node_name + "/module_id", module_id) || !n.getParam(node_name + "/bar_id", bar_id)) {
    std::cerr << "Parameter not found" << std::endl;
    return EXIT_FAILURE;
  }
  
  std::vector<int> t_init;
  std::vector<float> q_init;
  
  if (!n.getParam(node_name + "/t_init", t_init) ||
      !n.getParam(node_name + "/q_init", q_init)) {
    std::cerr << "Could not read parameter" << std::endl;
    return EXIT_FAILURE;
  }

  ros::Subscriber sub  = n.subscribe<tensegrity_state_estimation::Pose>("visual_pose_" + std::to_string(bar_id), 1000, chatterCallback);
  ros::Publisher pub = n.advertise<tensegrity_state_estimation::Pose>("module_pose_" + std::to_string(module_id), 1000);

  ros::Rate loop_rate(30);

  ukf_state_t init_state = {
    .t = (cv::Mat_<float>(3,1) << t_init[0], t_init[1], t_init[2]),
    .q = Quaternion(q_init[0],q_init[1],q_init[2],q_init[3]),
    .P = cv::Mat::eye(STATE_DIM-1,STATE_DIM-1, cv::DataType<float>::type)
  };

  ukf.init(init_state);

  cv::Mat tmp_t;
  Quaternion tmp_q;

  rotate_vect(init_state.q, module_offset.t, &tmp_t);
  tmp_t += ukf.state.t;
  module_pose.x = tmp_t.at<float>(0);
  module_pose.y = tmp_t.at<float>(1);
  module_pose.z = tmp_t.at<float>(2);

  tmp_q = init_state.q * module_offset.q;
  module_pose.qw = tmp_q.w;
  module_pose.qx = tmp_q.x;
  module_pose.qy = tmp_q.y;
  module_pose.qz = tmp_q.z;
  module_pose.id = module_id;

  g_semaphore = true;

  cv::waitKey(100);
  while (ros::ok())
  {
    
    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    if (g_semaphore) {
      // node_pose needs to be initialized to initial state and transformed into module frame
      pub.publish(module_pose);
      g_semaphore = false;
    }
    
    std::cout << "Hey:: " << pub.getNumSubscribers() << std::endl;

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}
